%global pypi_name tinyrpc

Summary:	A small, modular, transport and protocol neutral RPC library that, among other things, supports JSON-RPC and zmq.
Name:		python-%{pypi_name}
Version:	1.1.7
Release:	3%{?dist}
License:	MIT
URL:		http://github.com/mbr/%{pypi_name}
Source0:	%{pypi_source}

BuildArch:	noarch

%description
tinyrpc aims to do better by dividing the problem into cleanly interchangeable parts that allow 
easy addition of new transport methods, RPC protocols or dispatchers.

%package -n python3-%{pypi_name}
Summary:	A small, modular, transport and protocol neutral RPC library that, among other things, supports JSON-RPC and zmq.
Provides:	python-%{pypi_name}
BuildRequires:	python3-devel python3-setuptools

%description -n python3-%{pypi_name}
tinyrpc aims to do better by dividing the problem into cleanly interchangeable parts that allow 
easy addition of new transport methods, RPC protocols or dispatchers.

%prep
%autosetup -n %{pypi_name}-%{version}

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}

mv %{buildroot}%{python3_sitelib}/tests %{buildroot}%{python3_sitelib}/%{pypi_name}/

%files -n python3-%{pypi_name}
%{_docdir}/*
%{python3_sitelib}/%{pypi_name}
%{python3_sitelib}/%{pypi_name}*.egg-info

%changelog
* Tue Oct 22 2024 cunshunxia <cunshunxia@tencent.com> - 1.1.7-3
- move tests into our own site-packages dir.

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.7-2
- Rebuilt for loongarch release

* Sat Oct 07 2023 Upgrade Robot <upbot@opencloudos.org> - 1.1.7-1
- Upgrade to version 1.1.7

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.6-4
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.6-3
- Rebuilt for OpenCloudOS Stream 23.09

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 1.1.6-2
- Rebuilt for OpenCloudOS Stream 23.05

* Mon Apr 17 2023 Wang Guodong <gordonwwang@tencent.com> - 1.1.6-1
- initial build
